package main

import (
	"database/sql"

	"github.com/theredcameron/rest"
)

type MedicineHandler struct {
	medicineDB *Medicine
}

func MedHandlerInit(db *sql.DB) (*MedicineHandler, error) {
	medDB, err := MedicineInit(db)
	if err != nil {
		return nil, err
	}

	medicine := &MedicineHandler{
		medDB,
	}
	return medicine, nil
}

func (this *MedicineHandler) Routes() rest.Endpoints {
	return rest.Endpoints{
		{
			Description: "GetMedicines",
			Method:      "GET",
			Path:        "/api/GetMedicines",
			F:           this.medicineDB.GetMedicines,
		},
		{
			Description: "GetMedicine",
			Method:      "GET",
			Path:        "/api/GetMedicine/{id}",
			F:           this.medicineDB.GetMedicine,
		},
		{
			Description: "EditMedicine",
			Method:      "POST",
			Path:        "/api/EditMedicine",
			F:           this.medicineDB.EditMedicine,
		},
		{
			Description: "DeleteMedicine",
			Method:      "POST",
			Path:        "/api/DeleteMedicine/{id}",
			F:           this.medicineDB.DeleteMedicine,
		},
	}
}
