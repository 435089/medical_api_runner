package main

import (
	"database/sql"

	"github.com/theredcameron/rest"
)

type MedicineLedgerHandler struct {
	transDB *MedicineLedger
}

func MedLedgerHandlerInit(db *sql.DB) (*MedicineLedgerHandler, error) {
	transDB, err := MedicineLedgerInit(db)
	if err != nil {
		return nil, err
	}

	tranasction := &MedicineLedgerHandler{
		transDB,
	}
	return tranasction, nil
}

func (this *MedicineLedgerHandler) Routes() rest.Endpoints {
	return rest.Endpoints{
		{
			Description: "Get Transactions",
			Method:      "GET",
			Path:        "/api/GetTransactions",
			F:           this.transDB.GetTransactions,
		},
		{
			Description: "Get Medicine Transactions",
			Method:      "GET",
			Path:        "/api/GetMedTransactions/{medID}",
			F:           this.transDB.GetMedTransactions,
		},
		{
			Description: "Get Transaction",
			Method:      "GET",
			Path:        "/api/GetTransaction/{id}",
			F:           this.transDB.GetTransaction,
		},
		{
			Description: "Edit Transaction",
			Method:      "POST",
			Path:        "/api/EditTransaction",
			F:           this.transDB.EditTransaction,
		},
		{
			Description: "Delete Transaction",
			Method:      "POST",
			Path:        "/api/DeleteTransaction/{id}",
			F:           this.transDB.DeleteTransaction,
		},
	}
}
