package main

import (
	"database/sql"
	"encoding/json"
	"strconv"

	"github.com/theredcameron/rest"
	ledger "gitlab.com/435089/med_medicine_ledger"
)

type MedicineLedger struct {
	transDB *ledger.MedicineLedgerDB
}

func MedicineLedgerInit(db *sql.DB) (*MedicineLedger, error) {
	transDB, err := ledger.Open(db)
	if err != nil {
		return nil, err
	}
	return &MedicineLedger{
		transDB: transDB,
	}, nil
}

func (this *MedicineLedger) GetTransactions(r *rest.Request) (interface{}, error) {
	transactions, err := this.transDB.GetTransactions()
	if err != nil {
		return nil, err
	}
	return transactions, nil
}

func (this *MedicineLedger) GetTransaction(r *rest.Request) (interface{}, error) {
	id := r.Vars["id"]
	newId, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	transaction, err := this.transDB.GetTransaction(newId)
	if err != nil {
		return nil, err
	}
	return transaction, nil
}

func (this *MedicineLedger) GetMedTransactions(r *rest.Request) (interface{}, error) {
	id := r.Vars["medID"]
	newId, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	transactions, err := this.transDB.GetMedTransactions(newId)
	if err != nil {
		return nil, err
	}
	return transactions, nil
}

func (this *MedicineLedger) EditTransaction(r *rest.Request) (interface{}, error) {
	var transaction ledger.Transaction
	var transactions []ledger.Transaction
	err := json.Unmarshal(r.Body, &transaction)
	if err != nil {
		return nil, err
	}
	transactions[transaction.ID] = transaction
	this.transDB.CUTransactions(transactions)
	return "success", nil
}

func (this *MedicineLedger) DeleteTransaction(r *rest.Request) (interface{}, error) {
	id := r.Vars["id"]
	newId, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	this.transDB.DeleteTransaction(newId)
	return "success", nil
}
