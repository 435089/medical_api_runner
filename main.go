package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/theredcameron/rest"
)

func main() {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		panic(err)
	}

	medHandler, err := MedHandlerInit(db)
	if err != nil {
		panic(err)
	}

	transHandler, err := MedLedgerHandlerInit(db)
	if err != nil {
		panic(err)
	}

	headers := make(rest.Headers)

	headers["Access-Control-Allow-Origin"] = "*"

	router, err := rest.NewRouter(nil, headers, medHandler.Routes(), transHandler.Routes())
	if err != nil {
		panic(err)
	}

	fmt.Println("Starting api on port 8000")

	err = router.Start("8000")
	if err != nil {
		panic(err)
	}
}
