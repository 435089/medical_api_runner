package main

import (
	"database/sql"
	"encoding/json"
	"strconv"

	"github.com/theredcameron/rest"
	medicine "gitlab.com/435089/med_medicine"
)

type Medicine struct {
	medDB *medicine.MedicineDB
}

func MedicineInit(db *sql.DB) (*Medicine, error) {
	medDB, err := medicine.Open(db)
	if err != nil {
		return nil, err
	}
	return &Medicine{
		medDB: medDB,
	}, nil
}

func (this *Medicine) GetMedicines(r *rest.Request) (interface{}, error) {
	medicines, err := this.medDB.GetMedicines()
	if err != nil {
		return nil, err
	}
	return medicines, nil
}

func (this *Medicine) GetMedicine(r *rest.Request) (interface{}, error) {
	id := r.Vars["id"]
	newId, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	medicine, err := this.medDB.GetMedicine(newId)
	if err != nil {
		return nil, err
	}
	return medicine, nil
}

func (this *Medicine) EditMedicine(r *rest.Request) (interface{}, error) {
	var med medicine.Medicine
	var meds []medicine.Medicine
	err := json.Unmarshal(r.Body, &med)
	if err != nil {
		return nil, err
	}
	meds[med.ID] = med
	this.medDB.CUMedicines(meds)
	return "success", nil
}

func (this *Medicine) DeleteMedicine(r *rest.Request) (interface{}, error) {
	id := r.Vars["id"]
	newId, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	this.medDB.DeleteMedicine(newId)
	return "success", nil
}
