module medical_api_runner

go 1.13

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.2+incompatible // indirect
	github.com/michaeljs1990/sqlitestore v0.0.0-20190919182151-25d6d7f2e768 // indirect
	github.com/theredcameron/rest v0.0.0-20191226043245-9cde170b36e3
	gitlab.com/435089/med_medicine v1.0.0
	gitlab.com/435089/med_medicine_ledger v1.0.0
	google.golang.org/appengine v1.6.5 // indirect
)
